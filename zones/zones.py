import pandas as pd
import geopandas as gpd
from shapely.geometry import Point

GEOMETRY_TAG = 'geometry'


class Zones:
    def __init__(self):
        self.url = 'https://data.winnipeg.ca/api/geospatial/39ur-higg?method=export&format=Shapefile'

    def external(self):
        return gpd.read_file(self.url)

    def check_point(self, lat, lng):
        my_point = Point(lng, lat)
        zones = self.external()

        try:
            zone = zones[zones[GEOMETRY_TAG].contains(my_point)].values[0][2]
        except:
            zone = None

        return zone


